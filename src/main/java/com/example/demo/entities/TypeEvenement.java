package com.example.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor
public class TypeEvenement implements Serializable {
    @Id @GeneratedValue
    private long type_id;
    private String type;
/*    @OneToMany(mappedBy = "type",cascade = CascadeType.ALL)
    private Collection<Evenement> events;*/
}
