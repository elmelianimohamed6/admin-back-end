package com.example.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor
public class Evenement implements Serializable {
    @Id @GeneratedValue
    private long id;
    private String description;
    private String email;
    private boolean actif;
    @ManyToOne
    @JoinColumn(name = "type_id")
    private TypeEvenement type_id;
}
