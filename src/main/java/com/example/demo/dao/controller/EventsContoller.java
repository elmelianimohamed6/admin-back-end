package com.example.demo.dao.controller;

import com.example.demo.dao.EvenementRepository;
import com.example.demo.entities.Evenement;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class EventsContoller {

    private EvenementRepository eventsRepo;


    @GetMapping(path = "/all")
    public List<Evenement> allEvents(){
        return eventsRepo.findAll();
    }

    @PostMapping(path ="/addEvent")
    public Evenement addEvent(@RequestBody Evenement event){

        return eventsRepo.save(event);
    }

    @PutMapping(path ="/modifyEvent/{id}")
    public Evenement modifyEvent(@RequestBody Evenement event,@PathVariable long id){
        event.setId(id);
        return eventsRepo.save(event);
    }

    @GetMapping(path="/event/{id}")
    public Evenement getEvent(@PathVariable long id){
        return eventsRepo.findById(id).get();
    }

}
