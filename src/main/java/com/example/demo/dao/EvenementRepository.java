package com.example.demo.dao;

import com.example.demo.entities.Evenement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;


@RepositoryRestResource(path = "events")
@CrossOrigin("*")
public interface EvenementRepository extends JpaRepository<Evenement,Long> {
}
